package com.examen.dev.crud;
 
import java.util.List;
import java.util.Map;

import org.hibernate.SessionFactory; 


public interface ICrudDAO<T> {

	public SessionFactory getSessionFactory();
	
	public T persist(T t);
	
 	public T persistList(List<T> t) throws Exception ;
	
	public T findById(Class<T> type, Object id);
	
	public List<T> findAll(Class<T> type);

	public List<T> findByName(Class<T> type, String propertyName, Object value);

	public void delete(T t) throws Exception;
	
	public List<T> findByNamedQuery(String namedQuery, Map<String, Object> parameters);
	
	public T findObjectByNamedQuery(String namedQuery, Map<String, Object> parameters);

	@SuppressWarnings("rawtypes")
	public List findByNamedQueryTransformer(Class<?> type, String namedQuery, Map<String, Object> parameters);
	
	public Object findObjectByNamedQueryTransformer(Class<?> type, String namedQuery, Map<String, Object> parameters);
	
	//public String findByNamedQueryJson(String namedQuery, Map<String, Object> parameters);

	public int executeUpdateNamedQuery(String namedQuery, Map<String, Object> parameters);
	
//	public List<T> findByCriteria(Class<T> type, Filter filter);

	public List<Object[]> execQuery(String stQuery);
	
	//public Blob createBlob(MultipartFile multipartFile);
	
//	public Blob createBlob(InputStream inputStream,long size);
	
	//public Blob createBlob(byte [] array);
	
	//public String executeNativeQueryJson(String strQuery, Map<String, Object> parameters);
	
	@SuppressWarnings("rawtypes")
	public List findByNativeQueryTransformer(Class<?> type, String nativeQuery, Map<String, Object> parameters);
	
	public Object findObjectByNativeQueryTransformer(Class<?> type, String nativeQuery, Map<String, Object> parameters);
	
    public Object findObjectByNativeQuery(Class<?> type,String nativeQuery, Map<String, Object> parameters);

    public Integer updateByNativeQuery(String nativeQuery, Map<String, Object> parameters);
    
}