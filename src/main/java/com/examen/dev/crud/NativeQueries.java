package com.examen.dev.crud;
 
import com.examen.dev.util.Constants;

public class NativeQueries { 
 
	 
		public static class SQLCliente{
		
		public static final String  UPDATE_CLIENTE=" UPDATE cliente"
				 + " SET"
				 + " CL_ST_NOMBRES=:strNombres,"
				 + " CL_ST_APELLIDO_PATERNO =:strApellidoPaterno ,"
				 + " CL_ST_APELLIDO_MATERNO =:strApellidoMaterno ,"
				 + " CL_DT_FECHA_NACIMIENTO =:dtFechaNacimiento ,"
				 + " CL_ST_DIRECCION =:strDireccion ,"
				 + " CL_ST_CORREO_ELECTRONICO =:strCorreoElectronico ,"
				 + " CL_NR_SEXO =:intSexo  " 
				 + " WHERE CL_ID_CLIENTE_PK =:intClienteId ;";
		 
	 
		
		public static final String DELETE_CLIENTE = " DELETE FROM cliente"
				+ " WHERE CL_ID_CLIENTE_PK =:intClienteId ; " ; 
		 
		 
		 
		public static final String FIND_CLIENTE_BY_ID= 
				" select S.CL_ID_CLIENTE_PK as intId ,"
				+ " S.CL_ST_NOMBRES as strNombres,"
				+ " S.CL_ST_APELLIDO_PATERNO as strApellidoPaterno,"
				+ " S.CL_ST_APELLIDO_MATERNO as strApellidoMaterno , "
				+ " S.CL_DT_FECHA_NACIMIENTO as dtFechaNacimiento,	"
				+ " S.CL_ST_DIRECCION  as strDireccion ,"
				+ " S.CL_ST_CORREO_ELECTRONICO as strCorreoElectronico,"
				+ " S.CL_NR_SEXO as intSexo " 
				+ "  from cliente S" 
				+ "   where S.CL_ID_CLIENTE_PK =:intClienteId ; ";
		
		public static final String FIND_ALL_CLIENTE = 
				" select S.CL_ID_CLIENTE_PK as intId ,"
				+ " S.CL_ST_NOMBRES as strNombres,"
				+ " S.CL_ST_APELLIDO_PATERNO as strApellidoPaterno,"
				+ " S.CL_ST_APELLIDO_MATERNO as strApellidoMaterno , "
				+ " S.CL_DT_FECHA_NACIMIENTO as dtFechaNacimiento,	"
				+ " S.CL_ST_DIRECCION  as strDireccion ,"
				+ " S.CL_ST_CORREO_ELECTRONICO as strCorreoElectronico,"
				+ " S.CL_NR_SEXO as intSexo " 
				+ "  from cliente S" ;
		  
			 
		}  

}