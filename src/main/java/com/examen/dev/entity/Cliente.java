package com.examen.dev.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
 
/**
 * The persistent class for the servicio database table.
 * 
 */
@Entity
@NamedQuery(name="Cliente.findAll", query="SELECT c FROM Cliente c")
public class Cliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="CL_ID_CLIENTE_PK")
	private int clIdClientePk;

	@Column(name="CL_ST_NOMBRES")
	private String clStNombres;

	@Column(name="CL_ST_APELLIDO_PATERNO")
	private String clStApellidoPaterno; 
	
	@Column(name="CL_ST_APELLIDO_MATERNO")
	private String clStApellidoMaterno;

	@Temporal(TemporalType.DATE)
	@Column(name="CL_DT_FECHA_NACIMIENTO")
	private Date clDtFechaNacimiento;
	 
	@Column(name="CL_ST_DIRECCION")
	private String clStDireccion;
	
	 
	@Column(name="CL_ST_CORREO_ELECTRONICO")
	private String clStCorreoElectronico;
	 
	@Column(name="CL_NR_SEXO")
	private int clNrSexo;
	
	 
	
	public Cliente() {
	}



	public int getClIdClientePk() {
		return clIdClientePk;
	}



	public void setClIdClientePk(int clIdClientePk) {
		this.clIdClientePk = clIdClientePk;
	}



	public String getClStNombres() {
		return clStNombres;
	}



	public void setClStNombres(String clStNombres) {
		this.clStNombres = clStNombres;
	}



	public String getClStApellidoPaterno() {
		return clStApellidoPaterno;
	}



	public void setClStApellidoPaterno(String clStApellidoPaterno) {
		this.clStApellidoPaterno = clStApellidoPaterno;
	}



	public String getClStApellidoMaterno() {
		return clStApellidoMaterno;
	}



	public void setClStApellidoMaterno(String clStApellidoMaterno) {
		this.clStApellidoMaterno = clStApellidoMaterno;
	}



	public Date getClDtFechaNacimiento() {
		return clDtFechaNacimiento;
	}



	public void setClDtFechaNacimiento(Date clDtFechaNacimiento) {
		this.clDtFechaNacimiento = clDtFechaNacimiento;
	}



	public String getClStDireccion() {
		return clStDireccion;
	}



	public void setClStDireccion(String clStDireccion) {
		this.clStDireccion = clStDireccion;
	}



	public String getClStCorreoElectronico() {
		return clStCorreoElectronico;
	}



	public void setClStCorreoElectronico(String clStCorreoElectronico) {
		this.clStCorreoElectronico = clStCorreoElectronico;
	}



	public int getClNrSexo() {
		return clNrSexo;
	}



	public void setClNrSexo(int clNrSexo) {
		this.clNrSexo = clNrSexo;
	}

	 
}