package com.examen.dev.dao;
 
import java.util.ArrayList;  
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.examen.dev.model.bean.ClienteBean;  
import com.examen.dev.model.bean.UserSessionBean; 
import com.examen.dev.crud.ICrudDAO;
import com.examen.dev.crud.NativeQueries;
import com.examen.dev.entity.Cliente; 
import com.examen.dev.exception.BusinessException;
import com.examen.dev.exception.FatalException;

import org.springframework.stereotype.Repository;
@SuppressWarnings("unchecked")
@Repository
@Transactional(propagation = Propagation.REQUIRED)
public class ClienteDAOImpl implements ClienteDAO {

		@Resource  
		private ICrudDAO<Cliente> clienteCrudDAO;
  
  
	@Override
	public Integer saveCliente(ClienteBean objClienteBean, UserSessionBean objUserSessionBean)
			throws BusinessException, FatalException, Exception {

		Cliente objCliente = new Cliente ();
		objCliente.setClStNombres(objClienteBean.getStrNombres());
		objCliente.setClStApellidoPaterno(objClienteBean.getStrApellidoPaterno());
		objCliente.setClStApellidoMaterno(objClienteBean.getStrApellidoMaterno());
		objCliente.setClDtFechaNacimiento(objClienteBean.getDtFechaNacimiento());
		objCliente.setClStDireccion(objClienteBean.getStrDireccion());
		objCliente.setClStCorreoElectronico(objClienteBean.getStrCorreoElectronico());
		objCliente.setClNrSexo(objClienteBean.getIntSexo());  
		objCliente = this.clienteCrudDAO.persist(objCliente);
		 
		return objCliente.getClIdClientePk();	
    } 
	
	 

	@Override
	public void updateCliente(ClienteBean objClienteBean, UserSessionBean objUserSessionBean) throws BusinessException, FatalException, Exception {
        
        //creamos una variable para el mapeo del objeto como parametro
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("strNombres",objClienteBean.getStrNombres());
		parameters.put("strApellidoPaterno",objClienteBean.getStrApellidoPaterno());
		parameters.put("strApellidoMaterno",objClienteBean.getStrApellidoMaterno());
		parameters.put("dtFechaNacimiento",objClienteBean.getDtFechaNacimiento());
		parameters.put("strDireccion",objClienteBean.getStrDireccion());
		parameters.put("strCorreoElectronico",objClienteBean.getStrCorreoElectronico());
		parameters.put("intSexo",objClienteBean.getIntSexo()); 
		parameters.put("intClienteId", objClienteBean.getIntId());
		clienteCrudDAO.updateByNativeQuery(NativeQueries.SQLCliente.UPDATE_CLIENTE, parameters);
	
	}
 
	 

	@Override
	public void deleteCliente(Integer intClienteId, UserSessionBean objUserSessionBean)
			throws BusinessException, FatalException, Exception {
        
        Map<String, Object> parameters = new HashMap<String, Object>(); 
    	parameters.put("intClienteId", intClienteId);
        clienteCrudDAO.updateByNativeQuery(NativeQueries.SQLCliente.DELETE_CLIENTE, parameters);
        //perfilClienteCrudDAO.updateByNativeQuery(NativeQueries.SQLPerfilCliente.DELETE_PERFIL_SERVICIO_BY_SERVICIO, parameters);
	}
	
	//SERVICIO QUE MUESTRA UN Cliente POR ID_Cliente
	@Override
	public ClienteBean getCliente(Integer intClienteId) throws BusinessException, FatalException, Exception {
		
		Map<String, Object> parameter = new HashMap<String,Object>();
		parameter.put("intClienteId", intClienteId);
		String query=NativeQueries.SQLCliente.FIND_CLIENTE_BY_ID;
		ClienteBean objClienteBean = (ClienteBean) clienteCrudDAO.findObjectByNativeQuery(ClienteBean.class, query, parameter);
		
	 
		
		return objClienteBean;
	}
	
	//SERVICIO QU MUESTRA UNA LISTA DE TODOS LOS ClienteS
	@Override
	public List<ClienteBean> getListCliente() throws BusinessException, FatalException, Exception {
		List<ClienteBean> lstClienteBean = new ArrayList<ClienteBean>(); 
		String query=NativeQueries.SQLCliente.FIND_ALL_CLIENTE;
		lstClienteBean = clienteCrudDAO.findByNativeQueryTransformer(ClienteBean.class, query, null);
			
		 return  lstClienteBean ; 

	}
	 
 
		  
}
