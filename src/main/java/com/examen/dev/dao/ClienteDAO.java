package com.examen.dev.dao;

	import java.util.List;  
	import com.examen.dev.model.bean.ClienteBean; 
	
	import com.examen.dev.model.bean.UserSessionBean;
	import com.examen.dev.exception.BusinessException;
	import com.examen.dev.exception.FatalException;
	
	public interface ClienteDAO{
	public Integer saveCliente( ClienteBean objClienteBean, UserSessionBean objUserSessionBean) throws BusinessException, FatalException, Exception;
	 public void updateCliente(ClienteBean objClienteBean, UserSessionBean objUserSessionBean)throws BusinessException, FatalException, Exception;
	 
	public void deleteCliente(Integer intClienteId , UserSessionBean objUserSessionBean)throws BusinessException, FatalException, Exception;
	public ClienteBean getCliente(Integer intClienteId) throws BusinessException, FatalException, Exception;
	public List<ClienteBean> getListCliente() throws BusinessException, FatalException, Exception;
	 	  
 

}