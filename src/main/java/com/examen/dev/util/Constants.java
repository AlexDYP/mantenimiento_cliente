package com.examen.dev.util;

public class Constants {
	public static final String STR_AT = "@";
	public static final String STR_TAB = "\t";
	public static final String STR_BREAK = "\n";
	public static final String STR_EMPTY = "";
	public static final String STR_ASTERISK = "*";
	public static final String STR_LIST_CONTAINS_NO_DATA = "List contains no data";
	public static final String STR_COLON = ":";
	public static final String STR_COMMA = ",";
	public static final String STR_PIPE = "|";
	public static final String STR_SEPARATOR = ";|";
	public static final String STR_WHITE_SPACE = " ";
	public static final String STR_TRUE = "true";
	public static final String STR_FALSE = "false";
	public static final String DATETIMEFORMAT = "HH:mm:ss dd/MM/yyyy";
	public static final String DATEFORMAT = "dd/MM/yyyy";
	public static final String DATEFORMAT2 = "dd.MM.yyyy";
	public static final String DATEFORMAT3 = "yyyy-MM-dd";
	public static final String DATEFORMAT4 = "ddMMyyyy";
	public static final String DATEFORMAT5 = "dd-MM-yyyy HH:mm:ss";
	public static final String DOUBLEFORMAT = "#,###.##";
	public static final String DOUBLEFORMAT2 = "#'###.##";
	public static final String STR_PERIOD_PREFIX = "MMMYYYY";
	public static final String STR_REGEX_ONLYDIGITS = new String("[0-9]+");
	public static final String STR_GUION = "-";
	public static final String STR_UNDERLINE = "_";
	public static final String STR_EXCLAMATION = "!";
	public static final String STR_COMMILLAS = "\"";
	public static final String STR_SLASH = "/";
	public static final String STR_POINT = ".";
	public static final String STR_HEADER = "H";
	public static final String STR_DOT_DELIMATOR = "@";
	public static final String STR_HEADER_XML_FILE = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
	public static final Integer INT_NUMBER_ZERO = 0;
	public static final Integer INT_NUMBER_ONE = 1;
	public static final String STR_LOCAL_IP = "127.0.0.1";
	public static final String STR_USER_SESSION = "USER_SESSION";

	//public static final String DAY_FINAL_TO_PLANILLA = "25"; 
	//public static final Integer NRO_DAYS_OF_MONTH = 30;
	//public static final Integer HOURS_OF_WORK_BY_DAY = 8;
  
	public static final Integer MASCULINO = 12;
	public static final Integer FEMENINO = 13;

	// porcentajes en bd 
		public static class Porcentaje{
			public static final Integer IGV = 1;
			public static final Integer ESSALUD_COSTOS = 2;
			public static final Integer CTS = 3;
			public static final Integer VACACIONES = 4;
			public static final Integer GRATIFICACIONES = 5;
			public static final Integer DETRACCION = 6 ; 
			public static final Integer ESSALUD_APORTE = 7;
		}
		
		
	// porcentajes en bd 
	public static class Constante{
		public static final Integer UIT = 1;
		public static final Integer CAMBIO_DOLAR_SOLES= 2;  
		public static final Integer DIA_CIERRE_PLANILLA = 3;
		public static final Integer DIAS_DEL_MES= 30;
		public static final Integer HORAS_TRABAJADAS_X_SEMANA = 48 ; 
		public static final Integer HORAS_TRABAJADAS_X_DIA = 9 ; 
	}
	  
public static final String BLANK = "";
	
	/** The Constant COLON. */
	public static final String COLON = " : ";
	
	/** The Constant DASH. */
	public static final String DASH = " - ";
	
	/** The Constant KEY_TXNID. */
	public static final String KEY_TXNID = "TXNID";
	
	/** The Constant XPATH_TXNID. */
	public static final String XPATH_TXNID = "//transcationId";
	
	/** The Constant ENTRY. */
	public static final String ENTRY = "Entry";
	
	/** The Constant EXIT. */
	public static final String EXIT = "Exit";
	
	
	/** Códigos de mensajes **/
	public static final Integer INT_RESPONSE_CODE_SUCESS = 0;
	public static final Integer INT_RESPONSE_CODE_ERROR = -1;
	public static final Integer INT_RESPONSE_CODE_00100 = 100;
	public static final Integer INT_RESPONSE_CODE_00110 = 110;
	public static final Integer INT_RESPONSE_CODE_SESSION_EXPIRED = 200;

	public static class DefinitionsTable {
		public static final Integer CONCEPTO_PAGO = 10;
		public static final Integer DISTRITO = 11;
		public static final Integer ESTADO_CLIENTE = 12;
		public static final Integer ESTADO_CONTRATO_PERSONA = 13;
		public static final Integer ESTADO_SERVICIO = 14;
		public static final Integer FORMA_PAGO = 15;
		public static final Integer NIVEL_CONOCIMIENTO = 16;
		public static final Integer TIPO_CONOCIMIENTO = 17;
		public static final Integer TIPO_CONTACTO = 18;
		public static final Integer TIPO_DIRECCION = 19;
		public static final Integer TIPO_DOCUMENTO_PERSONA= 20;
		public static final Integer TIPO_DOCUMENTO_SERVICIO= 21;
		public static final Integer TIPO_EMPRESA= 22;
		public static final Integer TIPO_MODALIDAD= 23;
		public static final Integer TIPO_MONEDA= 24;
		public static final Integer TIPO_PENSION= 25;
		public static final Integer TIPO_PERSONA= 26;
		public static final Integer ESTADO_PERSONA= 27;
		public static final Integer ESTADO_CANDIDATO= 28;
		public static final Integer ESTADO_VISITA = 29;
		public static final Integer COMISION= 30;
		public static final Integer BANCO= 31;
		public static final Integer TIPO_PLANILLA= 32;
		public static final Integer ESTADO_PLANILLA= 33;
		public static final Integer MOTIVO_CESE=34;
		public static final Integer TIPO_DETALLE_LIQUIDACION=35;
		public static final Integer MOTIVO_CONTRATO = 36;
		public static final Integer TIPO_AREA_SERVICIO = 37;
		public static final Integer TIPO_DOCUMENTO_CLIENTE= 38;
		public static final Integer ESTADO_FACTURACION= 39;
		public static final Integer TIPO_DOCUMENTO_BAJA =40;
		public static final Integer TIPO_OTROS_PAGOS = 41;
		public static final Integer TIPO_DOCUMENTO_PAGO = 42 ; 
	}
	
	/** CONSTANTES **/
	public static  class TipoPersona {   //26
		public static final Integer PERSONA_JURIDICA =1 ;
		public static final Integer PERSONA_NATURAL =2 ;
	}

	public static class EstadoCliente {  //12
		public static final Integer ACTIVO =5 ;
		public static final Integer NO_ACTIVO = 6;
		public static final Integer ELIMINADO = 7;
	}
	
	public static class TipoDireccion { //19
		public static final Integer LEGAL =10;
		public static final Integer CASA = 11;
		public static final Integer TRABAJO = 12;
		public static final Integer CORRESPONDENCIA = 13 ;
		public static final Integer OTRA = 14;
	}
	
	/*
	public static class TipoDocumentoServicio { //21
		public static final Integer COSTOS = 20;
		public static final Integer PROPUESTA =21 ;
		public static final Integer ANEXOS =22 ;
		public static final Integer ORDEN_SERVICIO =23;
	}
	*/
	public static class EstadoServicio {   //14
		//NO SE DEBE DE MOSTRAR LOS DOCUMENTOS
		public static final Integer  MODIFICADO_X_CLIENTE = 29;	
		public static final Integer  EN_PROCESO = 30;  //RECIEN CREADO
		public static final Integer  PENDIENTE_APROBACION = 31;  //ENVIADO PARA LA EVALUACION DEL USUARIO DE RRHH
		public static final Integer  APROBADO= 32;    //EL USUARIO DE RRHH LO APROBO
		public static final Integer  NO_APROBADO= 33;  // EL USUARIO DE RRHH LO DESAPROBO  
		public static final Integer  RECHAZADA_CLIENTE= 34; //EL CLIENTE RECHAZO EL SERVICIO
		
		//SE DEBE DE MOSTRAR LOS DOCUMENTOS
		public static final Integer  TERMINADO=35;        //EL SERVICIO CONCLUYO (FECHA DE FIN DE SERVICIO)
		public static final Integer  CANCELADO=36;		//EL CLIENTE CANCELO EL SERVICIO	
		public static final Integer  ELIMINADO=37;		//EL SERVICIO FUE CANCELADO Y ELIMINADO
		public static final Integer  RECHAZADO_CANDIDATO=38;	//EL SERVICIO HA SIDO RECHAZADO POR UN CANDIDATO SELECCIONADO Y DEBE DE PASAR A LA PRIMERA FASE	
		public static final Integer  EN_CURSO=39;		//EL SERVICIO ESTA EJECUTANDOSE .
	 } 
	
	public static class EstadoPersona{  //27
		public static final Integer ELIMINADA =40;
		public static final Integer NO_ASIGNADO = 41;
	}
	
	public static class EstadoCandidato{ // 28
		public static final Integer ELIMINADO = 45;
		public static final Integer SELECCIONADO = 46 ;
		public static final Integer EN_ESPERA = 47 ;
		public static final Integer COLABORADOR = 48 ;
		public static final Integer LIBRE = 49 ;
		//public static final Integer NO_SELECCIONADO = 48 ; 
	}
	
	public static class TipoDocumentoPersona{  //20
		public static final Integer DECLARACION_JURADA_DOMICILIO= 50;        //Se imprime uno bajo la declaracion jurada
		public static final Integer	ANTECEDENTES_POLICIALES =51;  // Unico
		public static final Integer CERTIFICADOS_ACADEMICOS=52;		//	
		public static final Integer NO_ADEUDOS = 53; 
		public static final Integer DOC_CONFIDENCIALIDAD = 54;    //uno por cada empresa
		public static final Integer CONTRATO_PLANILLA= 55;					//uno por cada empresa
		public static final Integer EXONERACION_DE_CUARTA = 56;
		public static final Integer CURRICULUM = 57;
		public static final Integer DECLARACION_JURADA_ANTECEDENTES_PENALES = 58;
		public static final Integer CONTRATO_RRHH = 59;
		public static final Integer CONTRATO_FACTURA = 60;
		public static final Integer DECLARACION_JURADA_NO_ENTREGA_CERTIFICADO = 61;
		public static final Integer CONTRATO_PRACTICAS_PREPROFESIONALES = 62 ; 
	}
	 
	public static class EstadoContratoPersona{ //13
		public static final Integer EN_CURSO = 66;
		public static final Integer EN_ESPERA = 67;
		public static final Integer	ELIMINADO=68;
		public static final Integer TERMINADO=69;	
	}
	
	public static class TipoMoneda{ //24
		public static final Integer SOLES=70;
		public static final Integer DÓLARES=71;
	}
	
	public static class EstadoVisita  { //29
		public static final Integer EN_ESPERA = 75;
		public static final Integer ACEPTADO = 76;
		public static final Integer RECHAZADO = 77;	
	}
	
	public static class NivelConocimiento{ //16
		public static final Integer BAJO=80;
		public static final Integer MEDIO=81;
		public static final Integer ALTO=82;
	}
	
	public static class TipoPension{ //25
		public static final Integer ONP=90;
		public static final Integer AFP_PRIMA=91;
		public static final Integer AFP_PROFUTURO=92;
		public static final Integer AFP_INTEGRA=93;
		public static final Integer AFP_HABITAT=94;
		
	}	
	public static class TipoEmpresa{ //22
		public static final Integer  TOPSTRATEGIC = 100;
		public static final Integer  PG_S = 101;
	}
	
	/*
	public static class ConceptoPago{ //10
		public static final Integer SUELDO_MENSUAL=110;
		public static final Integer GRATIFICACION=111;
		public static final Integer CTS=112;
		public static final Integer LIQUIDACION=113;
	}
	*/
	public static class FormadePago{  //15
		public static final Integer EFECTIVO=120;
		public static final Integer CHEQUE=121;
		public static final Integer PAGARÉ=123;
		public static final Integer TRANSFERENCIA=124;
		public static final Integer DEPOSITO=125 ;
		
	}
	
	public static class Comision{ // 30
		public static final Integer MIXTA=130;
		public static final Integer FLUJO=131;
		public static final Integer OTRA=132;
	}
	
	public static class Banco{ //31
		public static final Integer BBVA_CONTINENTAL=140;
		public static final Integer BCP=141;
		public static final Integer INTERBANK=142;
		public static final Integer BANCO_FINANCIERO=143;
		public static final Integer SCOTIABANK=144;
		public static final Integer BANCO_DE_LA_NACION=145;
		public static final Integer OTRA=146;
	}
	
	public static class Distritos { // 11
		public static final Integer SURCO = 201;
		public static final Integer LINCE = 202;
		public static final Integer SAN_ISIDRO = 203;
		public static final Integer LA_VICTORIA = 204;
		public static final Integer SAN_BORJA = 205;
		public static final Integer MIRAFLORES = 206;
		public static final Integer JESUS_MARIA = 207;
	}
	public static class TipoModalidad { // 23
		public static final Integer PLANILLA = 150;
		public static final Integer RECIBO_X_HONORARIOS = 151;
		public static final Integer FACTURA = 152;
	}

	public static class TipoContacto { // 18
		public static final Integer FAMILIAR = 160;
		public static final Integer TRABAJO = 161;
		public static final Integer OTRO = 162;
		public static final Integer TRABAJADOR = 163;
	}
	
	public static class TipoConocimiento { // 17
		public static final Integer PROGRAMACION = 170;
		public static final Integer ANALISIS = 171;
		public static final Integer GESTION = 172;
		public static final Integer TESTING = 173;
		public static final Integer BASE_DE_DATOS = 174;		
	}
	
	 
	
	public static class EstadoPlanilla { // 
		public static final Integer GENERADA = 190; 			// se genera el excel con la data necesaria y se espera la conformidad de los datos
		public static final Integer CONFORME = 191;				// 	Se confirma que los datos son correctos  se guarda los datos en la BD
		public static final Integer NO_CONFORME = 192;			//  Se indica que los datos no estan correctos
		public static final Integer ESPERA_VERIFICACION = 193;	// se envio el aarchivo al contador y se espera la devolucion con la nueva data
		public static final Integer VERIFICADA = 194;			// cruce de data correcto 
		public static final Integer CON_INCONSISTENCIAS = 195;	// en el cruce de data existe errores
		public static final Integer FINALIZADA = 196;			// se registraron los montos y se imprimieron las boletas
		
	}
	
	public static class MotivoCese { // 34
		public static final Integer INCUMPLIMIENTO_CONTRATO= 250; 			// se genera el excel con la data necesaria y se espera la conformidad de los datos
		public static final Integer TERMINO_CONTRATO = 251;				// 	Se confirma que los datos son correctos  se guarda los datos en la BD
		public static final Integer RENUNCIA = 252;			//  Se indica que los datos no estan correctos
		 
	}
	
	public static class TipoPago{ // 35
		public static final Integer PAGO_MENSUAL = 180;
		public static final Integer GRATIFICACION = 181;
		public static final Integer CTS = 182;
		public static final Integer VACACIONES= 183;
		public static final Integer PAGO_X_SERVICIOS= 184;
		public static final Integer CTS_TRUNCAS = 260; 			// se genera el excel con la data necesaria y se espera la conformidad de los datos
		public static final Integer VAC_TRUNCAS = 261;				// 	Se confirma que los datos son correctos  se guarda los datos en la BD
		public static final Integer GRATIF_TRUNCAS = 262;			//  Se indica que los datos no estan correctos
	}
	
	public static class MotivoContrato { //36
		public static final Integer INCREMENTO_DE_ACTIVIDADES = 330 ;
		public static final Integer MOTIVO_CONTRATO1=100000 ;
		public static final Integer MOTIVO_CONTRATO2=100000 ;
	}
	
	public static class EstadoFacturacion{ //39
		public static final Integer FACTURADO = 500 ; 
		public static final Integer ANULADA = 501;
	}
	
	public static class Alfresco{
		public static final  String USER_ALFRESCO = "admin";
		public static final String PASSWORD_ALFRESCO = "12345";
	}
	
	public static class TipoDocumento{
		public static final Integer DOCUMENTO_SERVICIO = 500;
		public static final Integer DOCUMENTO_RECURSO= 501;
		public static final Integer DOCUMENTO_RECURSO_EMPRESA= 502;
		public static final Integer DOCUMENTO_PLANILLA = 503;
		public static final Integer DOCUMENTO_PLANTILLA = 504 ;
		public static final Integer DOCUMENTO_CLIENTE = 505 ;
	}

	public static class Operacion{
		public static final Integer SAVE = 600;
		public static final Integer UPDATE = 601;
		public static final Integer DELETE = 602;
	}
	
	public static class TemplateAlfresco{
		public static final String CONTRATO = "268e4991-89e4-4317-a0a4-ad30bd2f2698;1.0" ; //oficina
		//public static final String CONTRATO = "e1f3b2d6-8e35-425b-9e28-dd5f55da758c;1.0" ; //casa
		public static final String DJ_NO_ANTECEDENTES_PENALES ="a6fadcb4-e8b0-47f3-97f8-6f7e7a6af8fb;1.0";  //declaracion jurada
		//public static final String DJ_DE_DOMICILIO="87d74ff1-5531-4281-bb43-57825e256b91;1.0";  //domicilio odt
		public static final String DJ_DE_DOMICILIO="6f4fa08c-fc66-4e48-86a8-0e70072c2405;1.0"; //domiclio pdf
		public static final String DJ_DE_NO_ENTREGA_DE_CERTIFICADO_DE_RETENCION_DE_5TA = "";
		public static final String NO_ADEUDOS = "" ; 
		public static final String DOC_DE_CONFIDENCIALIDAD = "";
		public static final String EXONERACION_DE_4TA = "";
		public static final String BOLETA = "";
	}
	
	public static class AspectosAlfresco{
		public static final String ETIQUETA_RECURSO= "dr" ;
		public static final String ETIQUETA_SERVICIO= "ds" ;
		public static final String ETIQUETA_PLANTILLA= "p" ;
		public static final String ASPECTO_NAME_RECURSO  = "aspectoDocumentoRecurso" ;
		public static final String ASPECTO_NAME_SERVICIO = "aspectoDocumentoServicio" ;
		public static final String ASPECTO_NAME_PLANTILLA = "aspectoDocumentoPlantilla" ;
	}  
	
	public static class SpringSecurity {
	    public static final long ACCESS_TOKEN_VALIDITY_SECONDS = 5*60*60;
	    public static final String SIGNING_KEY = "devglan123r";
	    public static final String TOKEN_PREFIX = "Bearer ";
	    public static final String HEADER_STRING = "Authorization";
	}
	
	public static class TipoServicio {
		public static final Integer Destaque = 1000;
		public static final Integer LBTR = 1001;
		public static final Integer CAS =1002;
		public static final Integer Otro = 1003;
	}
	
	public static class TipoDocumentoPago{ 
		public static final Integer BOLETA_DE_PAGO = 270;
	 }
	
	public static class PagoConcepto{ 
		//tabla Pago-Concepto
		//ingresos
		public static final Integer VACACIONES_TRUNCA=1;
		public static final Integer GRATIFICACIONES_TRUNCA=2;
		public static final Integer BONIFICACIONES_TRUNCA=3;
		public static final Integer CONCEPTOS_NO_REMUNERADOS=4;
				
		//descuentos
		public static final Integer	TARDANZAS =  6;
		public static final Integer	ADELANTOS = 7;
		public static final Integer	DESCUENTOS_JUDICIALES = 8; 
		public static final Integer	COPAGO_EPS = 9; 
		public static final Integer	 RETENCION_5TA = 10 ;  
	}
	
//----------------------------------------------------------------------------------------------------	
	
	 

}
