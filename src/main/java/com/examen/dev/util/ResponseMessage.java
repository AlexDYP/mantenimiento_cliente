package com.examen.dev.util;


import java.util.List;
import org.springframework.http.HttpStatus;
import com.examen.dev.util.MessageConstants;

public class ResponseMessage{
	Integer code;
	String message;
	List<String> messageDetail;
	String type;
	Integer valor;
	HttpStatus httpStatus;
	Object data;

	
	
	public ResponseMessage(MessageConstants messageConstants, String message ) {
		this.code=messageConstants.idMessage();
		this.message=message;
		this.type=messageConstants.getTypeMessage();
		//2 lineas agregada ....
		this.httpStatus=messageConstants.getHttpStatus();
		this.valor=messageConstants.getHttpStatus().value();
		//this.reason=messageConstants.getHttpStatus().getReasonPhrase();
	}
	
	
	public ResponseMessage(MessageConstants messageConstants ) {
		this.code=messageConstants.idMessage();
		this.type=messageConstants.getTypeMessage();
		//this.message = msg.getMessage(messageConstants.codeMessage(),null, objLocale); 
		//2 lineas agregada ....

		this.httpStatus=messageConstants.getHttpStatus();
		this.valor=messageConstants.getHttpStatus().value();
		//this.reason=messageConstants.getHttpStatus().getReasonPhrase();
	}
	
	public ResponseMessage(MessageConstants messageConstants,List<String> messageDetail, String message ) {
		this.code=messageConstants.idMessage();
		this.message=message;
		this.type=messageConstants.getTypeMessage();
		this.messageDetail = messageDetail;
	}
	
	public ResponseMessage() { 
	}
	public ResponseMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getValor() {
		return valor;
	}
	public void setValor(Integer valor) {
		this.valor = valor;
	}
	

	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public List<String> getMessageDetail() {
		return messageDetail;
	}
	public void setMessageDetail(List<String> messageDetail) {
		this.messageDetail = messageDetail;
	}


	public HttpStatus getHttpStatus() {
		return httpStatus;
	}


	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}	
	
	
}
