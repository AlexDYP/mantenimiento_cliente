package com.examen.dev.util;

import org.springframework.http.HttpStatus;

public enum MessageConstants{
	EXITO(0, TipoMensaje.SUCCESS.getTipo(),"message.top.operacionExitosa",HttpStatus.OK),
	NO_CONTENT(110,TipoMensaje.SUCCESS.getTipo(),"message.top.noContent",HttpStatus.NO_CONTENT),
	GET_EXITO(1,  TipoMensaje.SUCCESS.getTipo(),"message.top.operacionExitosa",HttpStatus.OK ),
	DELETE_EXITO(2,TipoMensaje.SUCCESS.getTipo(),"message.top.deleteExitosa",HttpStatus.OK),
	UPDATE_EXITO(3,TipoMensaje.SUCCESS.getTipo(),"message.top.updateExitosa",HttpStatus.OK) 

	 
	
	
	;
	
	private Integer idMessage;
	private String codeMessage;
	private String typeMessage;
	private HttpStatus httpStatus;

	MessageConstants(Integer idMessage, String typeMessage,String codeMessage,HttpStatus httpStatus) {
		this.idMessage = idMessage;
		this.codeMessage = codeMessage;
		this.typeMessage = typeMessage;
		this.httpStatus = httpStatus;
	}
	
	public static MessageConstants getById(Integer id) {
	    for(MessageConstants e : values()) {
	        if(e.idMessage.equals(id)) return e;
	    }
	    return null;
	}
	
	public Integer idMessage() {
		return idMessage;
	}
	
	public String codeMessage() {
		return codeMessage;
	}

	
	public String getTypeMessage() {
		return typeMessage;
	}

	public void setTypeMessage(String typeMessage) {
		this.typeMessage = typeMessage;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}
    
}
