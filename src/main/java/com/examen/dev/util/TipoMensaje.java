package com.examen.dev.util;
 
public enum TipoMensaje {
	WARNING("warning"),
 	SUCCESS("success");
	private String tipo;
	private TipoMensaje(String tipo) {
		this.tipo = tipo;
	}
	public String getTipo() {
		return this.tipo;
	}		
}
