package com.examen.dev.exception;

import java.util.List;

import com.examen.dev.util.MessageConstants; 

public class BusinessException extends RuntimeException {
	
    private static final long serialVersionUID = 1L;
    private MessageConstants enumMessage;
    private List<String> messages;
    private Object[] params;

    public BusinessException(MessageConstants enumMessage,List<String> messages) {
        this.enumMessage = enumMessage;
        this.messages = messages;
    }

    public BusinessException(MessageConstants enumMessage) {
        this.enumMessage = enumMessage;
    }    
    
    public BusinessException(MessageConstants enumMessage, Object[] params) {
        this.enumMessage = enumMessage;
        this.params = params;
    }    
    
    public BusinessException(String msg) {
        super(msg);
    }

    public BusinessException(String msg, Object[] params) {
        super(msg);
        this.params = params;
    }
    
    public BusinessException(Throwable cause) {
    	super(cause);
    }    
    
    public BusinessException(String msg,Throwable cause) {
    	super(msg,cause);
    }     
    public BusinessException(String msg, Object[] params,Throwable cause) {
    	super(msg,cause);
    	this.params = params;
    }

	public Object[] getParams() {
		return params;
	}

	public void setParams(Object[] params) {
		this.params = params;
	}

	public MessageConstants getEnumMessage() {
		return enumMessage;
	}

	public void setEnumMessage(MessageConstants enumMessage) {
		this.enumMessage = enumMessage;
	}

	public List<String> getMessages() {
		return messages;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

}
