package com.examen.dev.exception;

public class FatalException extends RuntimeException {
	
    private static final long serialVersionUID = 1L;
    //private static Object[] params;

    public FatalException(String msg) {

        super(msg);
    }

    public FatalException(String msg, Object[] params) {

        super(msg);
    }
    
    public FatalException(Throwable cause) {
    	super(cause);
    }    
    
    public FatalException(String msg,Throwable cause) {
    	super(msg,cause);
    }     
    public FatalException(String msg, Object[] params,Throwable cause) {
    	super(msg,cause);
    	//this.params = params;
    }         

}
