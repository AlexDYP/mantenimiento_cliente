package com.examen.dev.model.bean;

import java.util.Date;

public class ClienteBean {
		private  Integer intId;
	    private  String strNombres;
	    private  String  strApellidoPaterno;
	    private  String  strApellidoMaterno;
	    private		Date 	 dtFechaNacimiento;
	    private  String  strDireccion;
	    private  String  strCorreoElectronico;
	    private  Integer  	 intSexo;
	    
	    
	    public ClienteBean(){
	    }
	    
		public Integer getIntId() {
			return intId;
		}
		public void setIntId(Integer intId) {
			this.intId = intId;
		}
		public String getStrNombres() {
			return strNombres;
		}
		public void setStrNombres(String strNombres) {
			this.strNombres = strNombres;
		}
		public String getStrApellidoPaterno() {
			return strApellidoPaterno;
		}
		public void setStrApellidoPaterno(String strApellidoPaterno) {
			this.strApellidoPaterno = strApellidoPaterno;
		}
		public String getStrApellidoMaterno() {
			return strApellidoMaterno;
		}
		public void setStrApellidoMaterno(String strApellidoMaterno) {
			this.strApellidoMaterno = strApellidoMaterno;
		}
		public Date getDtFechaNacimiento() {
			return dtFechaNacimiento;
		}
		public void setDtFechaNacimiento(Date dtFechaNacimiento) {
			this.dtFechaNacimiento = dtFechaNacimiento;
		}
		public String getStrDireccion() {
			return strDireccion;
		}
		public void setStrDireccion(String strDireccion) {
			this.strDireccion = strDireccion;
		}
		public String getStrCorreoElectronico() {
			return strCorreoElectronico;
		}
		public void setStrCorreoElectronico(String strCorreoElectronico) {
			this.strCorreoElectronico = strCorreoElectronico;
		}
		public Integer getIntSexo() {
			return intSexo;
		}
		public void setIntSexo(Integer intSexo) {
			this.intSexo = intSexo;
		} 
	    
	    
}
