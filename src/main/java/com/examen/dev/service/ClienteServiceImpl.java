package com.examen.dev.service;
 
import java.text.SimpleDateFormat;
import java.util.Date; 
import java.util.List; 
 

import org.springframework.stereotype.Service; 
 
import org.springframework.beans.factory.annotation.Autowired;

import com.examen.dev.model.bean.ClienteBean;  
import com.examen.dev.model.bean.UserSessionBean;
import com.examen.dev.util.MessageConstants; 
import com.examen.dev.dao.ClienteDAO; 
import com.examen.dev.exception.BusinessException;
import com.examen.dev.exception.FatalException;
import com.examen.dev.util.Constants;

@Service(value="clienteService")
public class ClienteServiceImpl implements ClienteService{
	 
	 @Autowired
	 private ClienteDAO clienteDAO;
 
	 
	@Override
	public Integer saveCliente(ClienteBean objClienteBean, UserSessionBean objUserSessionBean)
			throws BusinessException, FatalException, Exception { 
		 
		if(objClienteBean.getIntSexo()==null) {
			objClienteBean.setIntSexo(0);
		}
		Integer intClienteId = clienteDAO.saveCliente(objClienteBean, objUserSessionBean);
		
		 
		return intClienteId;
    }  
	 
	@Override
	public void updateCliente(ClienteBean objClienteBean, UserSessionBean objUserSessionBean) throws BusinessException, FatalException, Exception {
		 
		if(objClienteBean.getIntSexo()==null) {
			objClienteBean.setIntSexo(0);
		}
		clienteDAO.updateCliente(objClienteBean, objUserSessionBean);	
	}

	 
	 
	
	@Override
	public void deleteCliente(Integer intClienteId, UserSessionBean objUserSessionBean)
			throws BusinessException, FatalException, Exception {
         
		clienteDAO.deleteCliente(intClienteId, objUserSessionBean);
	}


	//SERVICIO QUE MUESTRA UN Cliente POR ID_Cliente
	@Override
	public ClienteBean getCliente(Integer intClienteId) throws BusinessException, FatalException, Exception {
		
		ClienteBean objClienteBean =  clienteDAO.getCliente(intClienteId);
		
		if (objClienteBean == null ) {
			objClienteBean = null;
			//throw  new BusinessException(MessageConstants.MODULO_SERVICIO_NOT_FOUND); 
		}
 
		return objClienteBean;
	}
	

	
	//SERVICIO QU MUESTRA UNA LISTA DE TODOS LOS ClienteS
	@Override
	public List<ClienteBean> getCliente() throws BusinessException, FatalException, Exception {
		List<ClienteBean> lstClienteBean = clienteDAO.getListCliente();
 
		if (lstClienteBean == null ) 
			//throw  new BusinessException(MessageConstants.MODULO_SERVICIO_NOT_FOUND); 
			lstClienteBean =null;
			return lstClienteBean;

	}
	
 
 
}
