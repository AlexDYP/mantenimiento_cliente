package com.examen.dev.controller;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest; 
  
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource; 
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.examen.dev.model.bean.ClienteBean;
import com.examen.dev.model.bean.UserSessionBean;
import com.examen.dev.service.ClienteService;
import com.examen.dev.util.ResponseMessage;
import com.examen.dev.util.Constants;
import com.examen.dev.util.MessageConstants; 
import com.examen.dev.exception.BusinessException;
import com.examen.dev.exception.FatalException;
//import com.examen.dev.model.bean.UserSessionBean;
//import com.yuri.sig.service.ProFormaService;  

@RestController
public class ClienteController {
	//private static final Logger log = Logger.getLogger(ClienteController.class); 
    @Autowired
	private MessageSource msg;
	
    @Autowired
    ClienteService clienteService ;
     
 
//============================================================================================================== 
 
//save Cliente

@RequestMapping(value = "/cliente", method = RequestMethod.POST)
public ResponseEntity<ResponseMessage> saveCliente(@RequestBody ClienteBean objClienteBean, UriComponentsBuilder ucBuilder, Locale objLocale,Principal principal) throws FatalException, Exception {
 ResponseMessage response = null;
 try{
     UserSessionBean objUserSession = new UserSessionBean();
    // UserAccount objUserAccount = new UserAccount();
    // objUserAccount.setUaIdUserAccountPk(1);
    // objUserSession.setObjUserAccount(objUserAccount);
     
    Integer intId = clienteService.saveCliente(objClienteBean, objUserSession);	
    String message = msg.getMessage(MessageConstants.EXITO.codeMessage(),null, objLocale); 
    response = new ResponseMessage(MessageConstants.EXITO,message );
    response.setData(intId);		    	
    return new ResponseEntity<ResponseMessage>(response, response.getHttpStatus());
    }
 	catch(BusinessException objBusinessException){
	    String message = msg.getMessage(objBusinessException.getEnumMessage().codeMessage(), null, objLocale);
	    response = new ResponseMessage(objBusinessException.getEnumMessage(),message);
	    return new ResponseEntity<ResponseMessage>(response, objBusinessException.getEnumMessage().getHttpStatus());

 		}
    }


 

//Update Cliente 
@RequestMapping(value = "/cliente/{intClienteId}", method = RequestMethod.PUT)
public ResponseEntity<ResponseMessage> updateCliente(@PathVariable Integer intClienteId, @RequestBody ClienteBean objClienteBean,  UriComponentsBuilder ucBuilder, Locale objLocale,Principal principal,HttpServletRequest request) throws FatalException, Exception {
     ResponseMessage response = null; 
     try{	    
		 	String message = msg.getMessage(MessageConstants.UPDATE_EXITO.codeMessage(),null, objLocale); 
	         UserSessionBean objUserSession = new UserSessionBean();
	        // UserAccount objUserAccount = new UserAccount();
	       //  objUserAccount.setUaIdUserAccountPk(1);
	       //  objUserSession.setObjUserAccount(objUserAccount);
	         
	         clienteService.updateCliente(objClienteBean, objUserSession);		    
	        response = new ResponseMessage(MessageConstants.UPDATE_EXITO,message); 
	        return new ResponseEntity<ResponseMessage>(response, response.getHttpStatus());
	    }
	 catch(BusinessException objBusinessException){
		    String message = msg.getMessage(objBusinessException.getEnumMessage().codeMessage(), null, objLocale);
	        response = new ResponseMessage(objBusinessException.getEnumMessage(),message);
	        return new ResponseEntity<ResponseMessage>(response, objBusinessException.getEnumMessage().getHttpStatus());
	    }
} 




//Delete Cliente

@RequestMapping(value = "/cliente/{intClienteId}", method = RequestMethod.DELETE)
public ResponseEntity<ResponseMessage> deleteCliente(@PathVariable Integer intClienteId,  UriComponentsBuilder ucBuilder, Locale objLocale,Principal principal,HttpServletRequest request) throws FatalException, Exception {
     ResponseMessage response = null; 
	 try{ 
		 	String message = msg.getMessage(MessageConstants.DELETE_EXITO.codeMessage(),null, objLocale); 
			UserSessionBean objUserSession = new UserSessionBean();
			//UserAccount objUserAccount = new UserAccount();
			//objUserAccount.setUaIdUserAccountPk(1);
			//objUserSession.setObjUserAccount(objUserAccount);
		
			clienteService.deleteCliente(intClienteId,objUserSession);	    
 		response = new ResponseMessage(MessageConstants.DELETE_EXITO,message);
	    		
	        return new ResponseEntity<ResponseMessage>(response,response.getHttpStatus());
 	}
	 	 catch(BusinessException objBusinessException){
 		String message = msg.getMessage(objBusinessException.getEnumMessage().codeMessage(), null, objLocale);
			response = new ResponseMessage(objBusinessException.getEnumMessage(),message);
			return new ResponseEntity<ResponseMessage>(response, objBusinessException.getEnumMessage().getHttpStatus());
 	
	 }   
}

//Get Cliente

@RequestMapping(value = "/cliente/{intClienteId}", method = RequestMethod.GET)
public ResponseEntity<ResponseMessage> getCliente(@PathVariable Integer intClienteId, Locale objLocale,Principal principal) throws FatalException, Exception {
    ResponseMessage response = null;
    try {
		String message = msg.getMessage(MessageConstants.GET_EXITO.codeMessage(),null, objLocale); 
		ClienteBean objClienteBean = clienteService.getCliente(intClienteId);
		response = new ResponseMessage(MessageConstants.GET_EXITO,message);
		response.setData(objClienteBean);
		return new ResponseEntity<ResponseMessage>(response,response.getHttpStatus());
	}
	catch (BusinessException objBusinessException) {
		System.out.println(" Error en el controller : " + objBusinessException.getEnumMessage().getHttpStatus().value());
		String message = msg.getMessage(objBusinessException.getEnumMessage().codeMessage(), null, objLocale);
		response = new ResponseMessage(objBusinessException.getEnumMessage(), message);
		return new ResponseEntity<ResponseMessage>(response, objBusinessException.getEnumMessage().getHttpStatus());
	}
}





	//Get List todos los Clientes
	
	@RequestMapping(value = "/cliente", method = RequestMethod.GET)
	public ResponseEntity<ResponseMessage> getCliente( Locale objLocale,Principal principal) throws FatalException, Exception {
	    ResponseMessage response = null;
	    try {
			String message = msg.getMessage(MessageConstants.GET_EXITO.codeMessage(),null, objLocale);
			List<ClienteBean> LstClienteBean = clienteService.getCliente();
			response = new ResponseMessage(MessageConstants.GET_EXITO,message);
			response.setData(LstClienteBean);
			return new ResponseEntity<ResponseMessage>(response, response.getHttpStatus());
		} catch (BusinessException objBusinessException) {
			String message = msg.getMessage(objBusinessException.getEnumMessage().codeMessage(), null, objLocale);
			response = new ResponseMessage(objBusinessException.getEnumMessage(), message);
			return new ResponseEntity<ResponseMessage>(response, objBusinessException.getEnumMessage().getHttpStatus());
		}
	}
	
	
	
}