package com.examen.dev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.persistence.EntityManagerFactory;

import org.hibernate.SessionFactory; 
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.context.annotation.Bean; 
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

//import com.TOPStrategic.dev.util.Constants;


import java.util.HashMap; 
import java.util.Map; 
 

@SpringBootApplication
public class DevApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevApplication.class, args);
	}

	
	@Autowired
	private EntityManagerFactory entityManagerFactory;
	
	 
	@Bean
	public SessionFactory SessionFactory() {
	    if (entityManagerFactory.unwrap(SessionFactory.class) == null) {
	        throw new NullPointerException("factory is not a hibernate factory");
	    }
	    return entityManagerFactory.unwrap(SessionFactory.class);
	}
	 
	//FALTA TERMINAR DE CONFIGURAR 
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**") 
            		//.allowedOrigins("http://18.222.186.203") // servidor
            		//.allowedOrigins("http://192.168.10.15") // servidor
            		//.allowedOrigins("http://localhost") // localhost deployment
            		.allowedOrigins("http://localhost:4200","http://localhost,") //localhost devlopment  
        			.allowedMethods("PUT", "DELETE","GET","POST");	
    				//.allowedHeaders("header1", "header2", "header3")
    				//.exposedHeaders("header1", "header2")
    				//.allowCredentials(false).maxAge(3600);
            }
        };  
    } 
   
}
