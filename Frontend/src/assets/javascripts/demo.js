export function test1(){
    console.log('Calling test 1 function');
}

function validarInput(input) {
    var ruc       = input.value.replace(/[-.,[\]()\s]+/g,""),
        resultado = document.getElementById("resultado"),
        existente = document.getElementById("existente"),
        valido;
     console-log("en validar input ..")   ;
    existente.innerHTML = "";
    
    //Es entero?    
    if ((ruc = Number(ruc)) && ruc % 1 === 0
    	&& rucValido(ruc)) { // ⬅️ ⬅️ ⬅️ ⬅️ Acá se comprueba
    	valido = "Válido";
        resultado.classList.add("ok");
    } else {
        valido = "No válido";
        resultado.classList.remove("ok");
        console-log("en el else ..")   ;
    }
        
    resultado.innerText = "RUC: " + ruc + "\nFormato: " + valido;
}

function rucValido(ruc) {
    console-log("en ruc valido ..")   ;
    //11 dígitos y empieza en 10,15,16,17 o 20
    if (!(ruc >= 1e10 && ruc < 11e9
       || ruc >= 15e9 && ruc < 18e9
       || ruc >= 2e10 && ruc < 21e9))
        return false;
    
    for (var suma = -(ruc%10<2), i = 0; i<11; i++, ruc = ruc/10|0)
        suma += (ruc % 10) * (i % 7 + (i/7|0) + 1);
    return suma % 11 === 0;
    
}