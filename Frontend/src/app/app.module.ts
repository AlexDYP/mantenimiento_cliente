import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from "./app.routing";
import { AppComponent } from "./app.component"; 
  
import { HTTP_INTERCEPTORS} from "@angular/common/http";
import { HttpClientModule } from '@angular/common/http' 
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';  
//import { Select2Module } from 'ng4-select2'; 
 @NgModule({
  declarations: [AppComponent],
  imports: [   
    NgbModule.forRoot(), 
    AppRoutingModule,
    FormsModule,
   // Select2Module,
    BrowserModule, 
    HttpClientModule
  ],
  entryComponents: [ 
],

  providers: [     ],
  bootstrap: [AppComponent]
})
export class AppModule {}
