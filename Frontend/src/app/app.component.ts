import { Component } from "@angular/core";
import { Injectable } from '@angular/core';
   
import { environment } from '../environments/environment'; 
import { NgModel } from '@angular/forms';

import { ActivatedRoute } from "@angular/router";
import { Cliente } from "../app/models/cliente"; 

import { ClienteService } from './services/cliente.service';
import { HttpErrorResponse } from "@angular/common/http";
 
import { NgbModal, ModalDismissReasons }  from '@ng-bootstrap/ng-bootstrap'; 
   
import 'rxjs/Rx';
@Component({
  selector: 'cf-root',
   
templateUrl: "./views/cliente.html",
  styles: [],
  providers :[ClienteService]
})

@Injectable() 
export class AppComponent { 
  
  private  api_url =  environment.apiUrl;
    private  id : number ;   
    public message: string;
    public fullError: any;    

    public responseMessage : string;  
    public cliente : Cliente = new Cliente(); 
    public clientes : Cliente[]=[];   
 
    public closeResult : string ; 
    public idCliente : number;
    public  modalRef : any; 
    public validado : boolean =true;

    public responseNotFoundDocumentos : string  ="";
    public responseNotSave : string = "";
      
    constructor(private route: ActivatedRoute,  private clienteService: ClienteService,  
      private modalService : NgbModal  
    ) { }

  ngOnInit() {
    this.refreshClientes();  
  }


//** ========================= GUARDAR CLIENTES  ===============================
guardarCliente(c : Cliente) {
  this.cliente=c; 
  this.validarData(this.cliente);
  if(this.validado==false){
    this.responseMessage = "Todos los Campos son  Obligatorios.";
  }
  else{
  this.clienteService.saveCliente$(this.cliente)
    .subscribe(this.responseSaveCliente.bind(this),this.catchError.bind(this)); 
  }
}

responseSaveCliente(data){
  this.responseMessage = "Se Guardo Correctamente";
  console.log(this.responseMessage);
  this.refreshClientes();
}

//** ========================= UPDATE PERFILES ===============================

public updateCliente(c : Cliente){ 
  this.cliente=c;
  this.validarData(this.cliente);
  if(this.validado==false){
    this.responseMessage = "Todos los Campos son  Obligatorios.";
  }
  else{
  this.clienteService.updateCliente(this.cliente, this.cliente.intId)
    .subscribe(this.responseUpdateCliente.bind(this),this.catchError.bind(this)); 
  }
  }
  
  private responseUpdateCliente(data){
  this.responseMessage = "Se Actualizo Correctamente.";
  console.log(this.responseMessage);
  this.refreshClientes();
  } 

  
//** ========================= ELIMINAR PERFILES ===============================
  
public deleteCliente(cliente :Cliente){
 this.clienteService.deleteCliente$(cliente)
    .subscribe(this.responseDeleteCliente.bind(this), this.catchError.bind(this));
}

responseDeleteCliente(data){
  this.responseMessage = "Se Eliminó Correctamente.";
this.responseMessage = data.message ; 
this.refreshClientes();
} 
 
validarData(c: Cliente){
  if(this.cliente.strNombres=="" || this.cliente.strNombres == null){
    this.validado=false;
  }
  if(this.cliente.strApellidoPaterno=="" || this.cliente.strApellidoPaterno== null){
    this.validado=false;
  }
  if(this.cliente.strApellidoMaterno=="" || this.cliente.strApellidoMaterno== null){
    this.validado=false;
  }
  if(this.cliente.strDireccion=="" || this.cliente.strDireccion== null){
    this.validado=false;
  }
  if(this.cliente.strCorreoElectronico=="" || this.cliente.strCorreoElectronico== null){
    this.validado=false;
  }
  if(this.cliente.dtFechaNacimiento==null || this.cliente.dtFechaNacimiento== null){
    this.validado=false;
  }
   
   
}
  //-------------- REFRESH TALE CLIENTES ----------------------------------------------------

  public refreshClientes(){

    this.clienteService.getAllListClientes()
      .subscribe(this.responseRefreshClientes.bind(this),this.catchError.bind(this));
}

private responseRefreshClientes(data){
if(data.data!= null){
  this.clientes= data.data; 
}
  else {
    this.responseNotFoundDocumentos = "No se recupero info ... ";
  } 
} 
   
  
//** ========================= SELECT CLIENTE ==================================================*/ 

/** MODAL EXTERNO CONTACTO */
    public selectCliente(c:Cliente){
      this.responseMessage="";
        this.cliente = Object.assign({}, c);
    }

 
/**FIN  EDIT Y UPDATE SERVICIO */

//**  MODAL */
openModal(content){
  this.modalRef=this.modalService.open(content,{keyboard:false, backdrop:'static'} );
  this.modalRef.result.then((result) => {
    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
}

private getDismissReason(reason: any): string {
if (reason === ModalDismissReasons.ESC) {
  return 'by pressing ESC';
} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
  return 'by clicking on a backdrop';
} else {
  return  `with: ${reason}`;
}
}

//** FIN MODAL */
 
//======================CONTROL DE ERRORES ==================================0
private catchError(err) {
  if (err instanceof HttpErrorResponse) {
    this.catchHttpError(err);
    // console.log("estas aca 3");
  } else {
    this.message = `Unknown error, text: ${err.message}`;
    // console.log("estas aca 4");
  }
  this.fullError = err;
}

private catchHttpError(err: HttpErrorResponse) {
  if (err.status == 404) {
    this.showNotFoundError();
    console.log("estas aca 5");
  } else {
    this.showServerError(err);
    // console.log("estas aca 6");
  }
}

private showNotFoundError() {
  this.message = `NOT FOUND data for id: ${this.id} !!!`;
  this.fullError = null;
  // console.log("estas aca 7");
}

private showServerError(err: HttpErrorResponse) {
  this.message = `Server returned code ${err.status}, text: ${
    err.statusText
  }`;
  this.fullError = err;
  // console.log("estas aca 8");
}
}


