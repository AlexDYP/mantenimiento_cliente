import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AppComponent } from "./app.component";
import { componentFactoryName } from "@angular/compiler";
import { Component } from "@angular/core/src/metadata/directives";
 
 

const routes: Routes = [  
    { path: "cliente", component : AppComponent } 
   
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
