import { Injectable } from '@angular/core';
import { Cliente } from '../models/cliente';  

import { HttpClient,  HttpResponse, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map'; 
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do' ;
import  'rxjs/add/operator/map' ;
import { environment } from "../../environments/environment";
import { HttpHeaders } from '@angular/common/http';
import { AppComponent } from '../app.component';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token',
    'Accept' : 'application/json'
  })
};
@Injectable()
export class ClienteService {

  private  api_url =  environment.apiUrl;
  private url = environment.apiUrl + "cliente";
   

  constructor(private http: HttpClient) { } 
 
      getAllListClientes() {
        return  this.http.get <Cliente []> (this.url);
      }
      
      public getClienteById$(intId: number): Observable<Cliente> {
        return this.http.get<Cliente>(this.url + "/"+ intId);
      }

      public saveCliente$(cliente: Cliente): Observable<any> {
      return this.http.post(this.url, cliente, httpOptions);
      }
 
      public deleteCliente$(cliente: Cliente): Observable<any> {
        return this.http.delete(this.url + "/" + cliente.intId, httpOptions);
      }

      public updateCliente(cliente : Cliente, id: number): Observable<any> {
        return this.http.put(this.url + "/" + id , cliente, httpOptions);      
      } 

      
} 