import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";  
 
import { ClienteComponent } from './cliente.component'; 
import { ClienteService } from '../../services/cliente.service'; 
import { VentasRoutingModule } from "./mantenimiento.routing"; 
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { NgxSpinnerModule } from 'ngx-spinner'; 
import { TextMaskModule } from 'angular2-text-mask'; 
import { NgxCurrencyModule } from "ngx-currency"; 
@NgModule({
  imports: [ //Select2Module,  
      NgxSpinnerModule,  TextMaskModule ,NgxCurrencyModule ,
    CommonModule, FormsModule, VentasRoutingModule, NgbModule.forRoot(),
  ],
  declarations: [ClienteComponent, 
      ] ,
         providers : []
})
export class MatenimientoModule { }
