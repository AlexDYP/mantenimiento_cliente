  
export class Cliente {
    intId : number;
    strNombres : string;
    strApellidoPaterno  :   string;
    strApellidoMaterno  :     string ;
    dtFechaNacimiento   :   Date ; 
    strDireccion    :       string;
    strCorreoElectronico    :   string;
    intSexo :               number;
}
